# 2. Résolution d'un système linéaire
M = Matrice(4,5)
M.load([6,-2,2,4,16,12,-8,6,10,26,3,-13,9,3,-19,-6,4,1,-18,-34])
print(M)
G = Gauss()
G.solve(M)
print(M)


# 3. Calcul de rang
M = Matrice(4, 4)
M.load([2,0,1,2,0,2,1,0,1,1,1,1,2,0,1,2])
print(M)
G = Gauss()
G.solve(M)
print(M)

def rang(M):
	rang = 0
	for i in range(min(M.hauteur, M.hauteur)):
		if abs(M[i,i]-1) < _PRECISION:
			rang += 1
	return rang
	
print(rang(M))
	