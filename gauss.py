class Gauss(Matrice):
	
	def __init__(self, hauteur=0, largeur=0, remplisage=lambda double_index:0, snapshot_auto=True, precision=10**-6):
		self.precision = precision
		super().__init__(hauteur, largeur, remplisage, snapshot_auto)

	def solve(self, M=None):
		
		if M is not None:
			self.hauteur = M.hauteur
			self.largeur = M.largeur
			self.raw = M.raw
			self.snap(True)
		
		# Première phase : on annule successivement chaque colonne pour se ramener à un système triangulaire
		for colonne_courante in range(min(self.largeur-1, self.hauteur)):
			ligne_courante = colonne_courante
													
			ligne_pivot_index = -1
			_i = ligne_courante
			while ligne_pivot_index < 0:
				# On quitte la boucle si on dépasse le dernier éléments
				if _i >= len(self.colonne(colonne_courante)):
					break
					
				# On cherche un élement différent de 0
				if abs(self.colonne(colonne_courante)[_i]) > self.precision:
					ligne_pivot_index = _i
				_i += 1
			
			# Si on ne trouve pas de pivot, la phase 1 est fini
			if ligne_pivot_index < 0:
				continue
			
			ligne_pivot = self.ligne(ligne_pivot_index)
			ligne_pivot.echanger(ligne_courante)
			ligne_pivot.dilater(1/ligne_pivot[colonne_courante])
			
			for _i in range(ligne_courante+1, self.hauteur):
				self.ligne(_i).transvecter(ligne_pivot.index, -self.ligne(_i)[colonne_courante])
		
		# Deuxième phase : on utilise chaque ligne pour annuler les colonnes du dessus
		for colonne_courante in range(min(self.largeur-1, self.hauteur)-1, -1, -1): # On parcourt à l'envers
			ligne_courante = colonne_courante
			ligne_pivot = self.ligne(ligne_courante)
			
			for _i in range(colonne_courante):
				self.ligne(_i).transvecter(ligne_pivot.index, -self.ligne(_i)[colonne_courante])
		
		# Si la méthode solve a été utilisé sur une matrice externe ont supprime tout lien entre Gauss et la matrice
		if M is not None:
			self = Gauss()
			
				
	def __repr__(self):
			chaine = ""
			for i in range(len(self)):
				ligne, colonne = self.double_index(i)
				if abs(round(self.raw[i]) - self.raw[i]) < self.precision:
					chaine += str(round(self.raw[i]))
				else:
					chaine += str(self.raw[i])
				if colonne == self.largeur-1:
					chaine += "\n"
				else:
					chaine += "\t"
			return chaine
